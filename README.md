## RouteTracker
* This Simple Web app is essentially a clone of Google Maps.  The Difference is that users can type in waypoints and get 
  a route that optimizes the order of the waypoints.
* * *
### SnapShot(s)
![IMAGE](https://i.imgur.com/9cwuRuJl.png?1)
* * *
### Prerequisites
```
This web app is not hosted on a webserver yet.
Must use your own Google Maps API key with "Maps Javascript API" and "Directions API" enabled.
Place your API key in the script tags within the header.

Example: 
<script async defer src = "https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_GOES_HERE&callback=initMap"></script>
```
* * *
### Issues
* Code needs to be cleaned up (issues with CSS and other UI code).
* Can no longer host on college webserver, must find a webserver.
* Need to Clean up the Javascript code
* Need to delete unnecessary/unused files.
* * *
### Considerations For Improvement
* Allow the user to divide up the inputted waypoints among an inputted amount of "Travelers", each with their own set of directions.
* Make it so that the user doesn't have to explicitly create a new field for a waypoint.
* Allow the user to download the directions into a text-file.
* * *
### Author(s)

* **Brenda Tang**
* **Moises Vargas**
* **Clay**
* **Michel**
* **Shane Laskowski**

### Software
* Google Maps API
* Javascript
* HTML
* CSS